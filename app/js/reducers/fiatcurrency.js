import {FIAT_CURRENCY_SELECTED, FIAT_CURRENCIES} from '../actions/fiatCurrencySelect';
const initial_state = {
    fiat_currency: FIAT_CURRENCIES[0] ,
};
export default function (state = initial_state, action) {
    switch (action.type) {
        case FIAT_CURRENCY_SELECTED:
            return {
                ...state,
                fiat_currency : action.payload
            };
            break;
    }
    return state;
}
