import {SUCCESS} from '../actions/cryptocurrency​List';
const initial_state = {
    all:[],
};
export default function loadList(state = initial_state, action) {
    switch (action.type) {
        case SUCCESS:
            return {
                ...state,
                all :[...action.payload]
            };
            break;
    }
    return state;
}
