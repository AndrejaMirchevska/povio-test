import {LOAD,SUCCESS,FAIL,CURRENCY_REMOVED} from '../actions/cryptocurrency​Select';
const initial_state = {
    id: null,
    data: null,
    status: null
};
export  default function setCryptocurency(state = initial_state, action) {
    switch (action.type) {
        case CURRENCY_REMOVED: 
            return{
                ...state,
                 id: null,
                data: null,
                status: null
            }
        case LOAD:
            return{
                ...state,
                status: "Loading.."
            }
        case SUCCESS:
            return {
                ...state,
                id : action.payload[0].id,
                data : action.payload[0],
                status: null
            };
        case FAIL:
            return{
                ...state,
                status: action.error.message
            }
            break;
    }
    return state;
}