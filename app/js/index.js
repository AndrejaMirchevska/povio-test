import 'babel-polyfill';
import React from 'react';
import ReactDOM from "react-dom";
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers,compose} from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist';
// import createLogger from 'redux-logger';
import App from './components/App';
import cryptocurrencyList from './reducers/cryptocurrencyList';
import selectedCryptocurrency from './reducers/cryptocurrencySelect';
import fiatCurrency from './reducers/fiatcurrency';
import fetchMiddleware from './fetchMiddleware.js';
// const logger = createLogger();

const allReducers = combineReducers({
 cryptocurrencyList,
 selectedCryptocurrency,
 fiatCurrency
});

const store = createStore(
    allReducers,
    compose(
    applyMiddleware(fetchMiddleware),
    autoRehydrate())
);

persistStore(store, {whitelist:['fiatCurrency']},()=>{
    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById('app')
    );
});


