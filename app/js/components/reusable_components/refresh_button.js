import React, {Component} from 'react';

class RefreshButton extends Component {
    render() {
        return (
           <button onClick={() => this.props.refresh()} className="refresh-btn btn btn-default btn-sm">
                <span className="glyphicon glyphicon-refresh"></span> refresh
           </button>
        );
    }
}


export default RefreshButton;
