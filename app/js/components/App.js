import React from 'react';
import CryptocurrencyList from '../containers/cryptocurrency-list';
import FiatCurrency from '../containers/fiat-currency';
const App = () => (
    <div>
        <FiatCurrency/>
        <CryptocurrencyList />
    </div>
);

export default App;
