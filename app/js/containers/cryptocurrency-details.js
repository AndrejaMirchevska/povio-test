import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {loadCryptocurrency,removeSelectedCurrency} from '../actions/cryptocurrency​Select';
import RefreshButton from '../components/reusable_components/refresh_button';

class CryptocurrencyDetails extends Component {
    
        componentWillReceiveProps(nextProps) {
         // chek if the fiat_currency is changed to update the view
         if (nextProps.fiatCryptocurency!=this.props.fiatCryptocurency)
              this.props.reloadCryptocurrency(this.props.selected.id,nextProps.fiatCryptocurency.toLowerCase());
     }

    render() {
        if (!!this.props.status) 
            return (<div> { this.props.status } </div>);
        return (
            <div>
                <button className="btn btn-default btn-sm" onClick={() => this.props.clearDetails()}>
                <span className="glyphicon glyphicon-home"></span></button>
				 <RefreshButton refresh={() => this.props.reloadCryptocurrency(this.props.selected.id,this.props.fiatCryptocurency.toLowerCase())}></RefreshButton>
                 <table className="table">
                    <tbody>
                    <tr><td>Rank:</td> <td>{this.props.selected.rank}</td></tr>
                    <tr><td>Name:</td> <td>{this.props.selected.name}</td></tr>
                    <tr><td>Symnol:</td><td>{this.props.selected.symbol}</td></tr>
                    <tr><td>24h volume:</td><td>{this.props.selected['24h_volume_'+this.props.fiatCryptocurency.toLowerCase()]}</td></tr>
                    <tr><td>Market cap:</td><td> {this.props.selected['market_cap_'+this.props.fiatCryptocurency.toLowerCase()]}</td></tr>
                    <tr><td>Price:</td><td>{this.props.selected.price_btc}</td></tr>
                    <tr><td>1h change:</td><td>{this.props.selected.percent_change_1h}</td></tr>
                    <tr><td>24h change:</td><td> {this.props.selected.percent_change_24h}</td></tr>
                    <tr><td>7d change:</td><td>{this.props.selected.percent_change_7d}</td></tr>
                    <tr><td>Total suply:</td><td>{this.props.selected.total_supply}</td></tr>
                    <tr><td>Available suply:</td><td>{this.props.selected.available_supply}</td></tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selected: state.selectedCryptocurrency.data,
        status:  state.selectedCryptocurrency.status,
        fiatCryptocurency: state.fiatCurrency.fiat_currency
    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        reloadCryptocurrency: loadCryptocurrency,
        clearDetails: removeSelectedCurrency
    }, dispatch);
}

export default connect(mapStateToProps,matchDispatchToProps)(CryptocurrencyDetails);
