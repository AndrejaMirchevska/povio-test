import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {loadCryptocurrency} from '../actions/cryptocurrency​Select';
import {loadCryptocurrencyList} from '../actions/cryptocurrency​List';
import CryptocurrencyDetails from '../containers/cryptocurrency-details';
import RefreshButton from '../components/reusable_components/refresh_button';

class CryptocurrencyList extends Component {

    componentWillReceiveProps(nextProps){
         //check if fiat_currency is changed to update the view
        if (nextProps.fiatCryptocurency != this.props.fiatCryptocurency && !this.props.detailsShowed)
            this.props.loadCryptocurrencyList(nextProps.fiatCryptocurency);
        //check if the details are hidden to update the view
        else if ((nextProps.detailsShowed != this.props.detailsShowed) && (!nextProps.detailsShowed))
            this.props.loadCryptocurrencyList(nextProps.fiatCryptocurency);
    }

    componentWillMount(){
        this.props.loadCryptocurrencyList(this.props.fiatCryptocurency);
    }

    renderList() {

        return this.props.cryptocurrencies.map((item) => {
            return (
                <tr key={item.id}
                    onClick={() => this.props.selectCryptocurrency(item.id,this.props.fiatCryptocurency)}>
                   <td> {item.rank}</td>
                   <td>{item.symbol}</td>
                   <td> {item['price_'+this.props.fiatCryptocurency.toLowerCase()]} </td>
                   <td>{item.percent_change_24h}%</td>                   
                </tr>
            );
        });
    }

    render() {
        if(!!this.props.detailsShowed)
        return(<CryptocurrencyDetails />);
        return (
            <div>
                <RefreshButton refresh={() => this.props.loadCryptocurrencyList(this.props.fiatCryptocurency)}></RefreshButton>
                <table className="table list">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>symbol</th>
                        <th>price in "{this.props.fiatCryptocurency}"</th>
                        <th>24h change </th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderList()}
                    </tbody>
                </table>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        cryptocurrencies: state.cryptocurrencyList.all,
        fiatCryptocurency: state.fiatCurrency.fiat_currency,
        detailsShowed: !!state.selectedCryptocurrency.id,
    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        selectCryptocurrency: loadCryptocurrency,
        loadCryptocurrencyList : loadCryptocurrencyList
    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(CryptocurrencyList);
