import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {FIAT_CURRENCIES, selectFiatCurrency} from '../actions/fiatCurrencySelect';

class FiatCurrency extends Component {
    constructor(){
        super();
        this.options = FIAT_CURRENCIES;
    }
    renderList() {
        return this.options.map((val,index) => {
            return (
                <option key= {index} value = {val} selected = {(this.props.selected == val)? true : ""}> {val} </option>      
            );
        });
    }
    change(event){
         this.props.selectFiatCurrency(event.target.value);
    }
    render() {
        return (
            <select onChange = { this.change.bind(this) }>
               {this.renderList()}
            </select>
        );
    }
}

function mapStateToProps(state) {
    return {
        selected: state.fiatCurrency.fiat_currency
    };
}
function matchDispatchToProps(dispatch){
    return bindActionCreators({
        selectFiatCurrency
    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(FiatCurrency);
