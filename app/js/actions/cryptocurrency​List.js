export const LOAD = "LOAD_CRYPTOCURRENCY_LIST";
export const SUCCESS = "LOAD_CRYPTOCURRENCY_LIST_S";
export const FAIL = "LOAD_CRYPTOCURRENCY_LIST_F";

export function loadCryptocurrencyList(fiat_currency) {
    return {
        types: [
            LOAD,
            SUCCESS,
            FAIL
        ],
        promise: {
            url: "https://api.coinmarketcap.com/v1/ticker/?limit=100&convert=" + fiat_currency,
            method: "GET"
        },
    };
}
