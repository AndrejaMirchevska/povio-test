export const LOAD = "LOAD_CRYPTOCURRENCY";
export const SUCCESS = "LOAD_CRYPTOCURRENCY_S";
export const FAIL = "LOAD_CRYPTOCURRENCY_F";

export function loadCryptocurrency(id,fiat_currency){
    return {
        types: [
            LOAD,
            SUCCESS,
            FAIL
        ],
        promise: {
            url: "https://api.coinmarketcap.com/v1/ticker/"+id+"/?convert=" + fiat_currency,
            method: "GET"
        },
    };
}
export const CURRENCY_REMOVED = "CURRENCY_REMOVED";
export const removeSelectedCurrency = () => {
    return {
        type: CURRENCY_REMOVED,
    }
};
