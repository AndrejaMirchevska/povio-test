export const FIAT_CURRENCIES = ["EUR", "USD","CNY"];
export const FIAT_CURRENCY_SELECTED = "FIAT_CURRENCY_SELECTED";

export const selectFiatCurrency = (currency) => {
    return {
        type: FIAT_CURRENCY_SELECTED,
        payload: currency
    }
};
